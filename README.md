# DCA

## Principe
Une tâche (cron) se déclenche périodiquement, et place des ordres d'achat de BTC et ETH avec les montants et la monnaie configurés.

## Utilisation
- Créer le fichier src/main/resources/application.yml, selon le modèle suivant (exemple d'achat de 100 € de BTC et ETH sur Binance, le lundi à 12h, et 10 € de BTC et ETH sur Coinbase, tous les jours)
```yaml
binance:
  enabled: true
  url: https://api.binance.com
  auth:
    api-key: <key>
    secret-key: <secret>
  buy:
    cron: "0 0 12 * * MON"
    fiat: EUH
    btc: 100
    eth: 100

coinbase:
  enabled: true
  url: https://api.pro.coinbase.com
  auth:
    key: <key>
    secret: <secret>
    passphrase: <passphrase>
  buy:
    cron: "0 0 12 * * *"
    fiat: EUR
    btc: 10
    eth: 10
```
- Builder le projet
```bash
mvn clean install
```
- Lancer le jar généré
```bash
cd target
java -jar dca-1.0-SNAPSHOT-spring-boot.jar
```

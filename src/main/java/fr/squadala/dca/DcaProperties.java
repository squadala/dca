package fr.squadala.dca;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.math.BigDecimal;

@Getter
@ConstructorBinding
@AllArgsConstructor
@ConfigurationProperties
public class DcaProperties {

    private final Binance binance;
    private final Coinbase coinbase;

    @Getter
    @ConstructorBinding
    @AllArgsConstructor
    public static class Binance {
        private final Auth auth;
        private final Buy buy;

        @Getter
        @ConstructorBinding
        @AllArgsConstructor
        public static class Auth {
            private final String apiKey;
            private final String secretKey;
        }
    }

    @Getter
    @ConstructorBinding
    @AllArgsConstructor
    public static class Coinbase {
        private final Auth auth;
        private final Buy buy;

        @Getter
        @ConstructorBinding
        @AllArgsConstructor
        public static class Auth {
            private final String key;
            private final String secret;
            private final String passphrase;
        }
    }

    @Getter
    @ConstructorBinding
    @AllArgsConstructor
    public static class Buy {
        private final String fiat;
        private final BigDecimal btc;
        private final BigDecimal eth;
    }

}

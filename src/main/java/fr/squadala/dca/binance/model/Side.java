package fr.squadala.dca.binance.model;

public enum Side {
    SELL,
    BUY
}

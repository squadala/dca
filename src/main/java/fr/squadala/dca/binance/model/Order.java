package fr.squadala.dca.binance.model;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class Order {
    private String symbol;
    private Side side;
    private Type type;
    private BigDecimal quoteOrderQty;
}

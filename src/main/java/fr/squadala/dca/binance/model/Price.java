package fr.squadala.dca.binance.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Price {
    private String symbol;
    private BigDecimal price;
}

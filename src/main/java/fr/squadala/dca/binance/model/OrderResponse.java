package fr.squadala.dca.binance.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class OrderResponse {
    private String symbol;
    private Long orderId;
    private Long orderListId;
    private String clientOrderId;
    private long transactTime;
}

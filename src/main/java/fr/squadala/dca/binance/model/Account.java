package fr.squadala.dca.binance.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class Account {
    private List<Balance> balances;
}

package fr.squadala.dca.binance.model;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString
public class Balance {
    private String asset;
    private BigDecimal free;
}

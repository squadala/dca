package fr.squadala.dca.binance.api;

import fr.squadala.dca.binance.model.Price;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        name = "binanceNoAuth",
        url = "${binance.url}")
public interface BinanceNoAuthClient {

    @GetMapping("/api/v3/ticker/price")
    Price getPrice(@RequestParam String symbol);
}

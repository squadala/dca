package fr.squadala.dca.binance.api;

import lombok.RequiredArgsConstructor;
import lombok.experimental.Delegate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BinanceClient implements BinanceNoAuthClient, BinanceApiKeyClient, BinanceApiKeyAndSignatureClient {

    @Delegate
    private final BinanceNoAuthClient noAuthClient;
    @Delegate
    private final BinanceApiKeyClient apiKeyClient;
    @Delegate
    private final BinanceApiKeyAndSignatureClient apiKeyAndSignatureClient;
}

package fr.squadala.dca.binance.api;

import feign.RequestInterceptor;
import fr.squadala.dca.DcaProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

@RequiredArgsConstructor
public class BinanceApiKeyAndSignatureClientConfiguration {

    private final DcaProperties properties;

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header("X-MBX-APIKEY", properties.getBinance().getAuth().getApiKey());
            requestTemplate.query("timestamp", String.valueOf(System.currentTimeMillis()));
            String queryString = requestTemplate.queryLine().substring(1);
            byte[] body = requestTemplate.body();
            String signature = sign(queryString, body);
            requestTemplate.query("signature", signature);
        };
    }

    private String sign(String queryString, byte[] body) {

        byte[] secret = properties.getBinance().getAuth().getSecretKey().getBytes(StandardCharsets.UTF_8);

        String bodyString = body != null ? new String(body, StandardCharsets.UTF_8) : "";
        String totalParams = queryString + bodyString;

        byte[] hmac = calcHmacSha256(secret, totalParams.getBytes(StandardCharsets.UTF_8));

        return String.format("%032x", new BigInteger(1, hmac));
    }


    private byte[] calcHmacSha256(byte[] secretKey, byte[] message) {
        byte[] hmacSha256;
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, "HmacSHA256");
            mac.init(secretKeySpec);
            hmacSha256 = mac.doFinal(message);
        } catch (Exception e) {
            throw new RuntimeException("Failed to calculate hmac-sha256", e);
        }
        return hmacSha256;
    }

}

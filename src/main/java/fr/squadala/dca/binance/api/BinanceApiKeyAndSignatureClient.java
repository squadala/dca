package fr.squadala.dca.binance.api;

import fr.squadala.dca.binance.model.Account;
import fr.squadala.dca.binance.model.Order;
import fr.squadala.dca.binance.model.OrderResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(
        name = "binanceApiKeyAndSignature",
        url = "${binance.url}",
        configuration = BinanceApiKeyAndSignatureClientConfiguration.class)
public interface BinanceApiKeyAndSignatureClient {

    @PostMapping("/api/v3/order")
    OrderResponse postOrder(@SpringQueryMap Order order);

    @GetMapping("/api/v3/account")
    Account getAccount();

}

package fr.squadala.dca.binance.api;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(
        name = "binanceApiKey",
        url = "${binance.url}",
        configuration = BinanceApiKeyClientConfiguration.class)
public interface BinanceApiKeyClient {

}

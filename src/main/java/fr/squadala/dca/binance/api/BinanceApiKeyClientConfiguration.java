package fr.squadala.dca.binance.api;

import feign.RequestInterceptor;
import fr.squadala.dca.DcaProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;

@RequiredArgsConstructor
public class BinanceApiKeyClientConfiguration {

    private final DcaProperties properties;

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> requestTemplate.header("X-MBX-APIKEY", properties.getBinance().getAuth().getApiKey());
    }
}

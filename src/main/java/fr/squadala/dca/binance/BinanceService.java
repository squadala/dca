package fr.squadala.dca.binance;

import fr.squadala.dca.DcaProperties;
import fr.squadala.dca.binance.api.BinanceClient;
import fr.squadala.dca.binance.model.Balance;
import fr.squadala.dca.binance.model.Order;
import fr.squadala.dca.binance.model.Side;
import fr.squadala.dca.binance.model.Type;
import fr.squadala.dca.common.AbstractExchangeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
@Slf4j
@ConditionalOnProperty("binance.enabled")
public class BinanceService extends AbstractExchangeService {

    private final BinanceClient client;
    private final DcaProperties properties;

    @Scheduled(cron = "${binance.buy.cron}")
    public void placeBuyOrders() {
        super.placeBuyOrders();
    }

    @Override
    protected DcaProperties.Buy getBuyProperties() {
        return properties.getBinance().getBuy();
    }

    @Override
    protected BigDecimal getAvailable(String fiat) {
        return client.getAccount().getBalances().stream()
                .filter(balance -> fiat.equals(balance.getAsset()))
                .findAny()
                .map(Balance::getFree)
                .orElse(BigDecimal.ZERO);
    }

    @Override
    protected void postOrder(String crypto, String fiat, BigDecimal quantity) {
        client.postOrder(Order.builder()
                .symbol(crypto + fiat)
                .quoteOrderQty(quantity)
                .side(Side.BUY)
                .type(Type.MARKET)
                .build());
    }
}

package fr.squadala.dca.common;

import fr.squadala.dca.DcaProperties;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
public abstract class AbstractExchangeService {

    protected void placeBuyOrders() {
        DcaProperties.Buy buy = getBuyProperties();
        String fiat = buy.getFiat();

        buy("BTC", fiat, buy.getBtc());
        buy("ETH", fiat, buy.getEth());
    }

    private void buy(String crypto, String fiat, BigDecimal quantity) {
        BigDecimal available = getAvailable(fiat);
        log.info("Balance : {} {}", available, fiat);

        if (quantity.compareTo(available) > 0) {
            log.error("NOT ENOUGH MONEY TO BUY {}, YOU POOR !!!", crypto);
        }
        postOrder(crypto, fiat, quantity);
        log.info("Bought {} {} of {}", quantity, fiat, crypto);
    }

    protected abstract DcaProperties.Buy getBuyProperties();

    protected abstract BigDecimal getAvailable(String fiat);

    protected abstract void postOrder(String crypto, String fiat, BigDecimal quantity);
}

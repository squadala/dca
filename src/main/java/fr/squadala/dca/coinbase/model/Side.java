package fr.squadala.dca.coinbase.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Side {
    @JsonProperty("sell")
    SELL,
    @JsonProperty("buy")
    BUY
}

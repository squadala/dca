package fr.squadala.dca.coinbase.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Type {
    @JsonProperty("limit")
    LIMIT,
    @JsonProperty("market")
    MARKET,
    @JsonProperty("stop")
    STOP
}

package fr.squadala.dca.coinbase.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Account {

    private String currency;
    private BigDecimal balance;
}

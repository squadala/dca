package fr.squadala.dca.coinbase.api;

import feign.RequestInterceptor;
import fr.squadala.dca.DcaProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Base64;

@RequiredArgsConstructor
public class CoinbaseClientConfiguration {

    private final DcaProperties properties;

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            String path = requestTemplate.path();
            String query = requestTemplate.queryLine();
            String requestPath = path + query;
            String method = requestTemplate.method();
            String timestamp = String.valueOf(Instant.now().getEpochSecond());
            byte[] body = requestTemplate.body();

            requestTemplate
                    .header("User-Agent","Java-SDK")
                    .header("CB-ACCESS-KEY", properties.getCoinbase().getAuth().getKey())
                    .header("CB-ACCESS-SIGN", sign(timestamp, method, requestPath, body))
                    .header("CB-ACCESS-TIMESTAMP", timestamp)
                    .header("CB-ACCESS-PASSPHRASE", properties.getCoinbase().getAuth().getPassphrase());
        };
    }

    private String sign(String timestamp, String method, String requestPath, byte[] body) {

        byte[] secret = Base64.getDecoder().decode(properties.getCoinbase().getAuth().getSecret());

        String bodyString = body != null ? new String(body, StandardCharsets.UTF_8) : "";
        String prehash = timestamp + method + requestPath + bodyString;

        byte[] hmac = calcHmacSha256(secret, prehash.getBytes(StandardCharsets.UTF_8));

        return Base64.getEncoder().encodeToString(hmac);
    }


    private byte[] calcHmacSha256(byte[] secretKey, byte[] message) {
        byte[] hmacSha256;
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, "HmacSHA256");
            mac.init(secretKeySpec);
            hmacSha256 = mac.doFinal(message);
        } catch (Exception e) {
            throw new RuntimeException("Failed to calculate hmac-sha256", e);
        }
        return hmacSha256;
    }

}

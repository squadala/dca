package fr.squadala.dca.coinbase.api;

import fr.squadala.dca.coinbase.model.Account;
import fr.squadala.dca.coinbase.model.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@FeignClient(
        name = "coinbase",
        url = "${coinbase.url}",
        configuration = CoinbaseClientConfiguration.class)
public interface CoinbaseClient {

    @GetMapping(value = "/accounts", produces = "application/json", consumes = "application/json")
    List<Account> listAccounts();

    @PostMapping(value="/orders", produces = "application/json", consumes = "application/json")
    void postOrder(Order order);
}

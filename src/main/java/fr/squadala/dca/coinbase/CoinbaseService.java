package fr.squadala.dca.coinbase;

import fr.squadala.dca.DcaProperties;
import fr.squadala.dca.coinbase.api.CoinbaseClient;
import fr.squadala.dca.coinbase.model.Account;
import fr.squadala.dca.coinbase.model.Order;
import fr.squadala.dca.coinbase.model.Side;
import fr.squadala.dca.coinbase.model.Type;
import fr.squadala.dca.common.AbstractExchangeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
@Slf4j
@ConditionalOnProperty("coinbase.enabled")
public class CoinbaseService extends AbstractExchangeService {

    private final CoinbaseClient client;
    private final DcaProperties properties;

    @Scheduled(cron = "${coinbase.buy.cron}")
    public void placeBuyOrders() {
        super.placeBuyOrders();
    }

    @Override
    protected DcaProperties.Buy getBuyProperties() {
        return properties.getCoinbase().getBuy();
    }

    @Override
    protected BigDecimal getAvailable(String fiat) {
        return client.listAccounts().stream()
                .filter(account -> fiat.equals(account.getCurrency()))
                .findAny()
                .map(Account::getBalance)
                .orElse(BigDecimal.ZERO);
    }

    @Override
    protected void postOrder(String crypto, String fiat, BigDecimal quantity) {
        client.postOrder(Order.builder()
                .productId(crypto + "-" + fiat)
                .funds(quantity)
                .side(Side.BUY)
                .type(Type.MARKET)
                .build());
    }
}
